﻿using UnityEngine;
using System.Collections;

public class StageClearController : MonoBehaviour {
    private bool _isClear;
    [SerializeField]
    private float _stageClearWaitTime = 3.0f;
    private float _timer;

    void Update()
    {
        if (_isClear)
        {
            _timer += Time.deltaTime;
            if (_timer > _stageClearWaitTime)
            {
                Debug.Log("Clear.");

                // Game Jam Menu Template が作ったオブジェクトを破棄する
                GameObject uiObject = GameObject.Find("UI");
                if (uiObject != null)
                    Destroy(uiObject);

                Application.LoadLevel("title");
            }
        }
    }

    public void Clear()
    {
        _isClear = true;
    }
}
