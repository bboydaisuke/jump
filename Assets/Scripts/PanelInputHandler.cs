﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

public class PanelInputHandler : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    PlayerMoveController _player;

    void Start()
    {
        if (_player == null)
            Debug.LogError("Player must be set in inspector: " + gameObject.name);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _player.Jump();
    }
}
