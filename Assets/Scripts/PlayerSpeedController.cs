﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PlayerSpeedController : MonoBehaviour
{
    [SerializeField]
    private UnityStandardAssets._2D.PlatformerCharacter2D _player;
    [SerializeField]
    private float _speedupPeriod = 30.0f;
    [SerializeField]
    private float _speedupUnit = 1.0f;
    [SerializeField]
    private UnityStandardAssets.Cameras.AutoCam _camera;
    private float _speedupPeriodCounter;
    [SerializeField]
    private AudioClip _speedupSound;
    private AudioSource _audioSrc;
    [SerializeField]
    private float _maxSpeed = 20.0f;
    private float _initialSpeed;
    [SerializeField]
    AudioClip _speedDownSound;


    void Start()
    {
        _audioSrc = GetComponent<AudioSource>();

        if (_player == null)
            Debug.LogError("Player must be set in inspector: " + gameObject.name);
        else
            _initialSpeed = _player.MaxSpeed;

        if (_camera == null)
            Debug.LogError("Camera must be set in inspector: " + gameObject.name);
    }

    void Update()
    {
        _speedupPeriodCounter += Time.deltaTime;
        if (_speedupPeriodCounter > _speedupPeriod)
        {
            Speedup(_speedupUnit);
            _speedupPeriodCounter = 0f;
        }
    }

    void Speedup(float AddSpeed)
    {
        if (_player.MaxSpeed < _maxSpeed)
        {
            _audioSrc.PlayOneShot(_speedupSound);
            _player.MaxSpeed += AddSpeed;
            Debug.Log("Speedup! Current speed: " + _player.MaxSpeed);
        }
        else
        {
            _audioSrc.PlayOneShot(_speedDownSound);
            _player.MaxSpeed = _initialSpeed;
        }

        // カメラのスピードも変える
        _camera.MoveSpeed = _player.MaxSpeed - 1.0f;
    }
}
