﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(UnityStandardAssets._2D.PlatformerCharacter2D), typeof(AudioSource))]
public class PlayerMoveController : MonoBehaviour {
    private UnityStandardAssets._2D.PlatformerCharacter2D _player;
    [SerializeField] private float _initialPlayerSpeed = 3.0f;
    private float _dirX = 1.0f;
    private bool _jump;
    // private Vector3 _savedPosition;
    private AudioSource _audioSrc;
    [SerializeField]
    private AudioClip _jumpSound;

	void Start () {
        _player = GetComponent<UnityStandardAssets._2D.PlatformerCharacter2D>();
        _player.MaxSpeed = _initialPlayerSpeed;
        _audioSrc = GetComponent<AudioSource>();
	}
	
    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Jump"))
            Jump();
    }

    void FixedUpdate()
    {
        //if (CheckIsStuck()) // ひっかかってしまって止まっていたら方向転換する
            //SwitchDirection(null);

        _player.Move(_dirX, false, _jump);
        _jump = false;

        //_savedPosition = transform.position;
	}

    /*
    bool CheckIsStuck()
    {
        Vector3 distance = transform.position - _savedPosition;
        if (Mathf.Abs(distance.x) < Mathf.Abs((_player.MaxSpeed * Time.fixedDeltaTime) / 30))
            return true;
        else
            return false;
    }
     */

    void OnCollisionEnter2D(Collision2D col)
    {
        SwitchDirection(col);
    }

    void OnCollisionStay2D(Collision2D col)
    {
        SwitchDirection(col);
    }

    void SwitchDirection(Collision2D col)
    {
        if (col != null)
        {
            Transform collisionParent = col.transform.parent;
            if (collisionParent.gameObject.name.Equals("RightWalls"))
                _dirX = -1.0f;
            else if (collisionParent.gameObject.name.Equals("LeftWalls"))
                _dirX = 1.0f;
        }
        else
            _dirX = _dirX * (-1);
    }

    public void Jump()
    {
        Debug.Log("Jump!");

        if (!_jump)
        {
            _audioSrc.PlayOneShot(_jumpSound);
            _jump = true;
        }
    }
}
